# AgileEngine backend-XML java test
Author: Walter Arroyo

It is built on top of [Jsoup](https://jsoup.org/).

This project is made to compare an original HTML page element like a button or link inside a div against sightly different pages simulating page evolution over the time.

This solution is based on accuracy level between the original and the different page done by a percentage table

If the target has:
 
* same ID it gives an accuracy of 25%
* same class it gives an accuracy of 10% 
* same title it gives an accuracy of 20% 
* same href it gives an accuracy of 20% 
* same href it gives an accuracy of 10% 
* same text it gives an accuracy of 15% 

for a total of 100% if the target element has all the coincidences at the same time.

you can use the following parameters to make it work:

Java -cp target.jar <absolute_path_to_original> <absolute_path_to_target> [optional] <element_id>

and it will show you 2 results, one for elements in the same path as the original to compare to any new or changed element within the same parent in the target page. And the other for any coincidence along the whole page.

the expected result is like 

```
  9:27:14 PM: Executing task 'App.main()'...
  
  :compileJava
  :processResources UP-TO-DATE
  :classes
  :App.main()
  [INFO] 2019-10-27 21:27:15,578 c.w.App - 
   Best coincidence for elements in the same parent 
   Accuracy: 10% 
   differences: [id: "" was "make-everything-ok-button", href: "#" was "#ok", title: "" was "Make-Button", onclick: "" was "javascript:window.okDone(); return false;", text: "View All Alerts" was "Make everything OK"] 
   XPATH: html> body> div[0]> div> div[2]> div> div> div 
   Element: <a href="#" class="btn btn-default btn-block">View All Alerts</a> 
  
  [INFO] 2019-10-27 21:27:15,579 c.w.App - 
   Best coincidence for elements in any parent 
   Accuracy: 40% 
   differences: [id: "" was "make-everything-ok-button", title: "Do-Link" was "Make-Button", text: "Do anything perfect" was "Make everything OK"] 
   XPATH: html> body> div[0]> div> div[2]> div> div> div[2] 
   Element: <a class="btn btn-success" href="#ok" title="Do-Link" rel="next" onclick="javascript:window.okDone(); return false;"> Do anything perfect </a> 
  
  
  BUILD SUCCESSFUL
  
  Total time: 0.581 secs
  9:27:15 PM: Task execution finished 'App.main()'.
```

This project is based on the Java Snippet template provided at test.

