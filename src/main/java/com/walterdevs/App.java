package com.walterdevs;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class App {
    private static Logger LOGGER = LoggerFactory.getLogger(App.class);

    private static String CHARSET_NAME = "utf8";

    public static void main(String[] args) {

        if (args.length < 2) {
            LOGGER.error("Arguments must be at least 2: arg 1 absolute path to the original HTML, arg2 absolute path to the diff HTML, arg3 (optional) target id from original");
            return;
        }
        String originalPath = args[0];
        String diffPath = args[1];
        String targetId = (args.length == 3) ? args[2] : "";

        Element originalElement = getOriginalElement(new File(originalPath), targetId);

        if (originalElement == null){
            LOGGER.error("Element in the original file with the id: {} is missing, change the id and try again", targetId);
            return;
        }
        Map<String, Object> bestSameParentCoincidence = new HashMap<>();
        Map<String, Object> bestAnyParentCoincidence = new HashMap<>();
        List<Map<String, Object>> sameParentOnlyCoincidences= getCoincidences(originalElement, new File(diffPath), true);
        List<Map<String, Object>> anyParentOnlyCoincidences = getCoincidences(originalElement, new File(diffPath), false);
        for (Map<String, Object> item : sameParentOnlyCoincidences) {
            if (bestSameParentCoincidence.get("accuracy") == null)
                bestSameParentCoincidence = item;
            else if (Integer.parseInt(item.get("accuracy").toString()) > Integer.parseInt(bestSameParentCoincidence.get("accuracy").toString()))
                bestSameParentCoincidence = item;
        }
        for (Map<String, Object> item : anyParentOnlyCoincidences) {
            if (bestAnyParentCoincidence.get("accuracy") == null)
                bestAnyParentCoincidence = item;
            else if (Integer.parseInt(item.get("accuracy").toString()) > Integer.parseInt(bestAnyParentCoincidence.get("accuracy").toString()))
                bestAnyParentCoincidence = item;
        }

        LOGGER.info("\n Best coincidence for elements in the same parent \n Accuracy: {}% \n differences: {} \n XPATH: {} \n Element: {} \n",
                    bestSameParentCoincidence.get("accuracy"),
                    bestSameParentCoincidence.get("differences"),
                    bestSameParentCoincidence.get("path"),
                    bestSameParentCoincidence.get("element"));

        LOGGER.info("\n Best coincidence for elements in any parent \n Accuracy: {}% \n differences: {} \n XPATH: {} \n Element: {} \n",
                    bestAnyParentCoincidence.get("accuracy"),
                    bestAnyParentCoincidence.get("differences"),
                    bestAnyParentCoincidence.get("path"),
                    bestAnyParentCoincidence.get("element"));
    }

    private static Element getOriginalElement(File htmlFile, String targetElementId) {
        try {
            String targetId = (!targetElementId.trim().equals("")) ? targetElementId : "make-everything-ok-button";
            return findElementById(htmlFile, targetId);
        }catch (Exception e) {
            return null;
        }
    }

    private static List<Map<String, Object>> getCoincidences(Element originalElement, File htmlFile, boolean samePath) {
        List<Map<String, Object>> coincidences = new ArrayList<>();
        String targetId = originalElement.id();
        String OriginalPath= getXPath(originalElement);
        Element Coincidence = null;
        String cssQuery = new StringBuilder()
                .append(originalElement.tag())
                .toString();
        try {
            Optional<Elements> elementsInHTML = findElementsByQuery(htmlFile, cssQuery);
            elementsInHTML.map(currentElement ->
               {
                   currentElement.iterator().forEachRemaining(element -> {
                       Integer accuracy = 0;
                       String elementPath = getXPath(element);
                       List<String> differences = new ArrayList<>();

                       if (element.id().equals(originalElement.id()))
                           accuracy += 25;
                       else
                           differences.add("id: \"" + element.id() + "\" was \"" + originalElement.id() + "\"");

                       if (originalElement.classNames().stream().parallel().anyMatch(element.className()::contains))
                           accuracy += 10;
                       else
                           differences.add("className: \"" + element.className() + "\" was \"" + originalElement.className() + "\"");

                       if (element.attr("href").equals(originalElement.attr("href")))
                           accuracy += 20;
                       else
                           differences.add("href: \"" + element.attr("href") + "\" was \"" + originalElement.attr("href") + "\"");

                       if (element.attr("title").equals(originalElement.attr("title")))
                           accuracy += 20;
                       else
                           differences.add("title: \"" + element.attr("title") + "\" was \"" + originalElement.attr("title") + "\"");

                       if (element.attr("onclick").equals(originalElement.attr("onclick")))
                           accuracy += 10;
                       else
                           differences.add("onclick: \"" + element.attr("onclick") + "\" was \"" + originalElement.attr("onclick") + "\"");

                       if (element.text().equals(originalElement.text()))
                           accuracy += 15;
                       else
                           differences.add("text: \"" + element.text() + "\" was \"" + originalElement.text() + "\"");

                       if (!elementPath.equals(OriginalPath))
                           accuracy = (samePath) ? 0 : accuracy;

                       if (accuracy > 0) {
                           Map<String, Object> coincidenceMapping = new HashMap<>();
                           coincidenceMapping.put("accuracy", accuracy);
                           coincidenceMapping.put("differences", differences);
                           coincidenceMapping.put("path", elementPath);
                           coincidenceMapping.put("element", element);
                           coincidences.add(coincidenceMapping);
                       }
                   });
                   return null;
               }
            );
        }
        catch (Exception e) {

        }
        return coincidences;
    }

    private static String getXPath ( Element element){
        List<String> xPath = new ArrayList<>();
//        xPath.add(element.tag().toString());
        element.parents().iterator().forEachRemaining(parent -> {
            Integer index = parent.elementSiblingIndex();
            String tag = parent.tag().toString().concat((parent.siblingElements().size() > 1)? "[" + index.toString() + "]" : "");
            xPath.add(0, tag);
        });
        return xPath.stream().collect(Collectors.joining("> "));
    }

    private static Optional<Elements> findElementsByQuery(File htmlFile, String cssQuery) {
        try {
            Document doc = Jsoup.parse(
                    htmlFile,
                    CHARSET_NAME,
                    htmlFile.getAbsolutePath());

            return Optional.of(doc.select(cssQuery));

        } catch (IOException e) {
            LOGGER.error("Error reading [{}] file", htmlFile.getAbsolutePath(), e);
            return Optional.empty();
        }
    }
    private static Element findElementById(File htmlFile, String targetElementId) {
        try {
            Document doc = Jsoup.parse(
                    htmlFile,
                    CHARSET_NAME,
                    htmlFile.getAbsolutePath());

            return doc.getElementById(targetElementId);

        } catch (IOException e) {
            LOGGER.error("Error reading [{}] file", htmlFile.getAbsolutePath(), e);
            return null;
        }
    }
}
